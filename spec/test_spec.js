function tested(a, b) {
    return a + b
}


function tested1(a, b) {
    return tested(a,a) + tested(b, b)
}



describe("Тест", function(){
    it("ожидание1", function(){
        expect( tested(1, 2) ).toBe(3)
    })

    it("ожидание2", function(){
        expect( tested1(1, 2) ).toBe(6)
    })

    it("ожидание3", function(){
        expect( tested(2, 2) ).toBe(4)
    })


})