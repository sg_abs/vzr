// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'

import Multiselect from 'vue-multiselect'
import Insuredadult from './components/InsuredAdult'

import _ from 'lodash'

Vue.config.productionTip = false

Vue.filter('serialNumber', function(index) {
    index = parseInt(index)

    var serialNumbers = ['первого','второго','третьего','четвертого','пятого']
    return serialNumbers[ index ]
});

Vue.filter('serialNumber2', function(index) {
    index = parseInt(index)

    var serialNumbers = ['первый','второй','третий','четвертый','пятый']
    return serialNumbers[ index ]
});


Vue.filter('ucFirst', function(word) {
  if ( !word ) return ''

  return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase()
});

window.app = new Vue({
  el: '#content',
  components: {
    Multiselect,
    Insuredadult
  },
  data: {
    selected_countries: [],
    countries: [],
    travel: {
      date_begin: '',
      date_end: '',
      type: 'once',
      me_days: 3,
      period: 30
    },
    calc_pers: [{
      age: '0',
      sport: false
    }],
    calculator: {
      sp_id: 0,
      programms: [],
      errors: [],
      selected_programm: null,
      risks_models: null,
      result: null,
      fetching: false
    },
    polis: {
      insurer: {
        lastname: '',
        name: '',
        patronymic: '',
        lastname_lat: '',
        name_lat: '',
        phone: '+7 ',
        email: '',
        birthday: '',
        is_citizen: false,
        sport: false,
        sport_types: '',
        is_insured: false,
        childs: []
      },
      insureds: [{
        lastname: '',
        name: '',
        patronymic: '',
        lastname_lat: '',
        name_lat: '',
        phone: '+7 ',
        email: '',
        birthday: '',
        is_citizen: false,
        sport: false,
        sport_types: '',
        is_insured: false,
        childs: []
      }],
      accept: false,
      accept_error: '',
      promocode: {
        has: false,
        promocode: '',
        description: ''
      }

    },
    validate_errors: [],
    dadata_token: '477f940f0a9800303084db27a269dbe7da290767',
    wait_response: false
  },
  methods: {
    scrollToBlock(block) {
        var titleBlockPos = $(block).offset().top,
            menuBlockHeight = $('#main-menu').outerHeight(),
            scrollPosition = titleBlockPos - menuBlockHeight;

        $('html, body').animate({
            scrollTop: scrollPosition
        }, 100)
    },
    scrollToTopForm() {
        $('.st2').trigger('click');
        this.scrollToBlock('.title_block');
    },
    fetchCountries() {
      var vm = this;
      $.ajax({
        url: '/profile/GetCountries_json.php',
        method: 'GET',
        success(res){
          vm.countries = res;
        }
      })
    },
    addInsToCalc() {
      if ( this.calc_pers.length < 5 ) {
        this.calc_pers.push({
          age: '0',
          sport: false
        })

        this.fetchInsurancePrograms()
      }
    },
    changeRisks(){
      var vm = this

      vm.prepareRisks()

      vm.fetchInsurancePrograms()
    },
    changeInsSum() {
      var vm = this

      vm.prepareRisks()
      vm.calculateForm()
    },
    removeInsFromCalc(idx) {
      if ( 1 < this.calc_pers.length && idx < this.calc_pers.length ) {
        this.calc_pers.splice(idx, 1)
      }
    },
    changeDates() {
      if ( !isNaN(this.daysCount) ) {
        this.fetchInsurancePrograms()
      }
    },
    fetchInsurancePrograms() {
      var vm = this
      this.calculator.fetching = true

      vm.calculator.programms = []
      vm.calculator.errors = []

      var query = this.queryForCalculator

      $.ajax({
        method: 'POST',
        url: '/profile/akbars/vzp_mv/get_form_options.php',
        data: query,
        success(res) {
          vm.calculator.fetching = false
          if ( res.response.hasOwnProperty('errors') ) {
            vm.calculator.errors = res.response.errors
          } else {
            vm.calculator.programms = res.response
            // vm.calculator.selected_programm = res.response[0]
            vm.calculator.sp_id = 0
            vm.prepareRisks()
          }
          // vm.calculator.fetching = false

          vm.calculateForm()
        },
        error(res) {
          vm.calculator.fetching = false
        }
      })
    },
    calculateForm() {
      var vm = this,
          data = {}

      data.name_str = $('input[name=name_str]').val()
      data.country_groups = vm.selectedCountriesGroups
      data.days = vm.daysCount
      data.insureds = vm.calc_pers
      data.risks = vm.selectedRisks
      data.travel_type = vm.travel.type

      $.ajax({
          method: 'POST',
          url: '/profile/akbars/vzp_mv/calc_multi_user.php',
          contentType: "application/json; charset=utf-8",
          dataType: "json",
          data: data,
          success: function(res) {
            vm.calculator.result = res
          }
      });
    },
    prepareRisks() {
      var vm = this,
          risks = []

      if ( vm.selectedProgram == undefined )
        return risks

      risks[1] = { selected: true, value: vm.selectedProgram.summ_med, id: 1 }
      _.each(vm.selectedProgram.risks, function(value, key){
        risks[ value.id ] = { selected: false, value: value.avialabel_sums[0], id: value.id }
      })

      vm.calculator.risks_models = risks
    },
    getPersonModel(idx) {
      if ( idx == 0 && this.polis.insurer.is_insured )
        return this.polis.insurer

      return this.polis.insureds[idx]
    },
    addPersonToForm() {
      if ( this.canAddPersonToForm ) {
        this.polis.insureds.push({
          lastname: '',
          name: '',
          patronymic: '',
          lastname_lat: '',
          name_lat: '',
          phone: '+7 ',
          email: '',
          birthday: '',
          is_citizen: false,
          sport: false,
          sport_types: '',
          is_insured: false,
          childs: []
        })
      }
    },
    removePersonFromForm(args) {
      var idx = args.person_id
      //  0 < idx excludes first person model deletion
      if ( 0 < idx && idx < this.polis.insureds.length ) {
        this.polis.insureds.splice(idx, 1)
      } 

    },
    addChildToPerson(person) {
      if ( this.canAddChild ) {
        this.getPersonModel(person).childs.push({
          lastname: '',
          name: '',
          patronymic: '',
          lastname_lat: '',
          name_lat: '',
          phone: '+7 ',
          email: '',
          birthday: '',
          address_full: '',
          address_full_lat: '',
          address_region: '',
          address_district: '',
          address_city: '',
          address_street: '',
          address_house: '',
          address_apartment: '',
          sport: false,
          sport_types: '',
          postal_code: ''
        })
      }
    },
    checkPromocode() {
      var data = $('#vzrBankForm').serialize(),
          vm = this

      vm.polis.promocode.description = ''

      $.ajax({
        url: '/profile/akbars/checkPromocode.php',
        data: data,
        method: 'POST',
        success: function(res) {
          var _result = JSON.parse(res)

          vm.polis.promocode.description = _result.message
        }
      })
    },
    changeLatName() {
      var lat_name = this.translate(this.polis.insurer.name)
      this.polis.insurer.name_lat = lat_name.toUpperCase();
    },
    changeLatLastname(){
      var lastname_lat = this.translate(this.polis.insurer.lastname)
      this.polis.insurer.lastname_lat = lastname_lat.toUpperCase();
    },
    translate(name) {
      var transl=new Array();
      transl['А']='A';     transl['а']='a';
      transl['Б']='B';     transl['б']='b';
      transl['В']='V';     transl['в']='v';
      transl['Г']='G';     transl['г']='g';
      transl['Д']='D';     transl['д']='d';
      transl['Е']='E';     transl['е']='e';
      transl['Ё']='Yo';    transl['ё']='yo';
      transl['Ж']='Zh';    transl['ж']='zh';
      transl['З']='Z';     transl['з']='z';
      transl['И']='I';     transl['и']='i';
      transl['Й']='Y';     transl['й']='y';
      transl['К']='K';     transl['к']='k';
      transl['Л']='L';     transl['л']='l';
      transl['М']='M';     transl['м']='m';
      transl['Н']='N';     transl['н']='n';
      transl['О']='O';     transl['о']='o';
      transl['П']='P';     transl['п']='p';
      transl['Р']='R';     transl['р']='r';
      transl['С']='S';     transl['с']='s';
      transl['Т']='T';     transl['т']='t';
      transl['У']='U';     transl['у']='u';
      transl['Ф']='F';     transl['ф']='f';
      transl['Х']='Kh';    transl['х']='kh';
      transl['Ц']='Ts';    transl['ц']='ts';
      transl['Ч']='Ch';    transl['ч']='ch';
      transl['Ш']='Sh';    transl['ш']='sh';
      transl['Щ']='Sch';   transl['щ']='sch';
      transl['Ъ']="";      transl['ъ']="";
      transl['Ы']='Y';     transl['ы']='y';
      transl['Ь']='';      transl['ь']='';
      transl['Э']='E';     transl['э']='e';
      transl['Ю']='Yu';    transl['ю']='yu';
      transl['Я']='Ya';    transl['я']='ya';

      var name_lat = '';

      for(var i=0;i<name.length;i++) {
          if(transl[name[i]]!=undefined) { name_lat += transl[name[i]]; }
          else { name_lat+=name[i]; }
      }

      return name_lat;
    },
    buildQuery() {
      var query = {},
          vm = this

        query.name_str = $('input[name=name_str]').val()
        query.countries = vm.selectedCountriesCodes
        query.date_begin = vm.travel.date_begin
        query.days = vm.daysCount
        query.ref = $('input[name=ref]').val()
        query.travel_type = vm.travel.type
        query.travel_period = vm.travel.period

        query.insurer = {
          surname :       vm.polis.insurer.lastname,
          name:           vm.polis.insurer.name,
          patronymic:     vm.polis.insurer.patronymic,
          latin_surname:  vm.polis.insurer.lastname_lat,
          latin_name:     vm.polis.insurer.name_lat,
          birthday:       vm.polis.insurer.birthday,
          phone:          vm.polis.insurer.phone,
          email:          vm.polis.insurer.email,
          is_citizen:     vm.polis.insurer.is_citizen
        }

        var _insureds = this.openedInsuredList
        
        query.insureds = _.map(_insureds, function(insured, insured_idx){
          var _insured = {
            surname:        insured.lastname,
            name:           insured.name,
            patronymic:     insured.patronymic,
            latin_surname:  insured.lastname_lat,
            latin_name:     insured.name_lat,
            birthday:       insured.birthday,
            is_citizen:     insured.is_citizen,
            summ:           vm.selectedProgram.summ_med,
            sport:          insured.sport.toString(),
          }

          if( insured.phone !== undefined ) 
            _insured.phone = insured.phone
          
          if (  insured.parent_id !== undefined )
            _insured.parent_id = insured.parent_id

          return _insured
        })

        query.risks = this.selectedRisks


        if ( this.polis.promocode.has ) {
          query.promocode = {
            code: this.polis.promocode.promocode,
            type: 10
          }
        }

        return query
    },
    normalName(name) {
      var newName = name
                      .replace(/[^а-яА-ЯёЁ-\s]/gim, '')
                      .replace(/\s+/g,' ')
                      .trim()
                      .toLowerCase()
                      .replace(' —', '-')
                      .replace('— ', '-')
                      .replace(' -', '-')
                      .replace('- ', '-')
  
      console.log(newName)

      newName = newName
                  .split(' ')
                  .map(
                      function(el){
                          var n = el.split('-')
                                      .map(
                                          function(ell){
                                              console.log("'" + ell + "'")
                                              if ( ell.length == 0 )
                                                  return ell
                                              ell = ell.trim()
                                              return ell.substr(0,1).toUpperCase() + ell.substr(1)
                                          }
                                      ).join("-")
                          return n
                      }
                  )
                  .join(' ')
  
      return newName
    },
    normalPName( pname ) {
      return pname.substr(0, 1).toUpperCase() + pname.substr(1) //.toLowerCase()
    },
    showErrors(errors){
      var vm = this

      _.forEach(errors, function(c_error){
        var inputSelector = ''
        var personPosition = null
        var isTextarea = false
        if (  c_error.parent == 'insurer' ) {

          if (  c_error.field == 'region' || c_error.field == 'np' || c_error.field == 'street' || c_error.field == 'house' ) {
            inputSelector = 'textarea[name=fulladress]'
          }else {
            inputSelector = 'input[name=' + c_error.field + ']'                      
          }
        } else if ( c_error.parent == 'insured' ) {

          var personPosition = vm.getPersonAbsolutePosition(c_error.item)
          personPosition.parent = personPosition.parent == 0 ? personPosition.parent : personPosition.parent - 1

          personPosition.parent = c_error.item

          if (  c_error.field == 'region' || c_error.field == 'np' || c_error.field == 'street' || c_error.field == 'house' ) {
            inputSelector = 'insfulladress_' + personPosition.parent
            isTextarea = true
          } else {
            inputSelector = 'ins' + c_error.field + '_' + personPosition.parent
          }
          if ( personPosition.child != -1 )
            inputSelector += '_child_' +  personPosition.child

          inputSelector = (isTextarea ? 'textarea' : 'input') + '[name=' + inputSelector + ']'
        }

        if ( inputSelector != '' )
          $(inputSelector).addClass('wrong_input')

        console.log(inputSelector)

      })

      vm.polis.accept_error = ''
    },
    validateForm() {
      var vm = this
      if ( !vm.polis.accept ) {
        vm.polis.accept_error = 'Необходимо ваше согласие на обработку персональных данных'
      } else {
        vm.polis.accept_error = ''

        if ( vm.blockRequest ) {
          return false
        }

        vm.wait_response = true

        var query = vm.buildQuery()
        $.ajax({
          method: "GET",
          url: '/profile/akbars/vzp_mv/validate_multi_wa.php',
          data: 'data=' + JSON.stringify(query),
          success: function( res ) {
            $('.wrong_input').removeClass('wrong_input')

            if ( res.status == 0 ) {
              vm.wait_response = false
              location.href = res.confirmUrl
            } else {
              var ce = false

              // vm.validate_errors = res.errors
              vm.showErrors(res.errors)

              vm.validate_errors.length = 0

              vm.polis.accept_error = 'Допущены ошибки при заполнении формы'
              res.errors.forEach(function(c_error){
                if ( c_error.field == 'citizen' && !ce ) {
                  vm.validate_errors.push('Страхователь и застрахованные должны быть гражданами РФ')
                  ce = true
                }

                if ( c_error.field == 'age' ) {
                  vm.validate_errors.push('Страхование доступно лицам до 70 лет (включительно)')
                  ce = true
                }

              })

              vm.wait_response = false

              var rmb = function() {
                $(this).removeClass('wrong_input')                
              }

              $('.wrong_input').change(rmb)
              $('.wrong_input').keyup(rmb)
            }
          }
        })
      }
    },
    getPersonAbsolutePosition(idx) {
      var vm = this

      var pair = {
        parent: null,
        child: null
      }

      var last_parent = idx

      for( var i = idx; i >= 0; i-- ) {
        if ( i == vm.crossLinks[i] ) {
          last_parent = i
          break
        }
      }

      pair.parent = last_parent
      pair.child = (idx - last_parent - 1)

      return pair
    },
    showModal(idx) {
        $('.tip').hide();
        $('.tip' + idx).show();
        $('.show_tip').trigger('click');
    },
    setPickerForEndDate() {
      console.log('setPickerForEndDate')
      var vm = this,
      dateEndPicker = $('input[name=Data_end]')
      dateEndPicker.datepicker('destroy')

      dateEndPicker.datepicker({
        language: 'ru-RU',
        format: 'dd.mm.yyyy',
        autoHide: true,
        startDate: moment(vm.travel.date_begin, 'DD.MM.YYYY').add(2, 'days').format('DD.MM.YYYY')
      })

      dateEndPicker.on('pick.datepicker', function(e){
        vm.travel.date_end = $(this).datepicker('getDate', true)
        vm.changeDates()
      })
      console.log('setPickerForEndDate1')
    },
    setPickerForBeginDate() {
      var vm = this

      var dateBeginPicker = $('input[name=Data_begin]'),
            dateEndPicker = $('input[name=Data_end]')

      vm.setPickerForEndDate()

      dateBeginPicker.datepicker({
        language: 'ru-RU',
        format: 'dd.mm.yyyy',
        autoHide: true,
        startDate: moment().add(1, 'days').format('DD.MM.YYYY')
      })

      dateBeginPicker.on('pick.datepicker', function(e){
        console.log( 'begin date changed' )
        var pickedDate =  $(this).datepicker('getDate', true)
        vm.travel.date_begin = pickedDate

        var endDate = vm.travel.date_end

        var endDatePickedNewOptions = {
          language: 'ru-RU',
          format: 'dd.mm.yyyy',
          autoHide: true
        }

        var mPickedStartDate    = moment( pickedDate, 'DD.MM.YYYY' ),
            mEndDate            = moment( endDate, 'DD.MM.YYYY' )

        dateEndPicker.datepicker('destroy')

        endDatePickedNewOptions.startDate = mPickedStartDate.add(2, 'days').format('DD.MM.YYYY')
        dateEndPicker.datepicker(endDatePickedNewOptions)


        if ( mPickedStartDate < mEndDate ) {
          dateEndPicker.datepicker('setDate', endDate)
        } else {
          dateEndPicker.datepicker('setDate', endDatePickedNewOptions.startDate)
        }

      })
    },
    switchTravelMode(mode) {
      //mode == 'once'     - 2 inputs
      //mode == 'multiple' - input + select
      console.log(mode)
      var vm = this
      if ( mode == 'once' ) {
        setTimeout(function(){
          vm.setPickerForEndDate()
        }, 500)
      } else {
        $('input[name=Data_end]').datepicker('destroy')
      }

      vm.calculateForm()
    }
  },
  computed: {
    thisInsureds() {
      var _insureds = []

    },
    openedInsuredList() {
      var _insureds = [],
        vm = this

      _.forEach(this.polis.insureds, function(person, person_id) {
          var person_model = (person_id == 0 ? vm.getPersonModel(0) : person)

          var _person = _.clone(person_model),
              _childs = _person.childs
          _person.childs = []

          _insureds.push(_person)

          if ( _childs.length > 0 ) {
            _.forEach(_childs, function(_child, child_id) {
              _child.parent_id = person_id
              _insureds.push(_child)
            })
          }
      })

      return _insureds
    },
    selectedProgram() {
      return this.calculator.programms[ this.calculator.sp_id ]
    },
    selectedRisks() {
      var vm = this

      var selected = _.filter(vm.calculator.risks_models, { selected: true })

      return _.map(selected, function(risk){
        return {
          id: risk.id,
          sum: risk.value
        }
      });
    },
    beginDateValid() {
      var _date = this.travel.date_begin,
          _mdate = moment( _date, 'DD.MM.YYYY' ),
          today = moment( moment().format('DD.MM.YYYY'), 'DD.MM.YYYY' )

      return _date.length == 10 && _mdate.isValid() && ( _mdate.diff( today, 'days' ) > 0 )
    },
    endDateValid() {
      var _date = this.travel.date_end,
          _bdate = this.travel.date_begin,
          _mdate = moment( _date, 'DD.MM.YYYY' )
      return _date.length == 10 && _mdate.isValid() && ( _mdate.diff( moment(), 'days' ) > 0 ) && ( _mdate.diff( moment(_bdate, 'DD.MM.YYYY'), 'days' ) > 0 )
    },
    beginDateErrors() {
      var _date_val = this.travel.date_begin,
          _date     = moment(_date_val, 'DD.MM.YYYY'),
          errors    = [],
          today = moment( moment().format('DD.MM.YYYY'), 'DD.MM.YYYY' )

      if ( _date_val.length != 10 || !_date.isValid()  ) {
        errors.push('Неправильная дата.')
      } else {
        if ( _date.diff( today, 'days' ) < 0 ) {
          errors.push('Поле даты начала страхования содержит прошедшую дату.')
        } else if ( _date.diff( today, 'days' ) < 1 ) {
          errors.push('Дата начала страхования должна быть не ранее, чем 3й день от даты оформления страхового договора.')
        }
      }

      return errors
    },
    endDateErrors() {
      var _date_val = this.travel.date_end,
          _date     = moment(_date_val, 'DD.MM.YYYY'),
          _bdate    = moment( this.travel.date_begin, 'DD.MM.YYYY' ),
          errors    = [],
          today = moment( moment().format('DD.MM.YYYY'), 'DD.MM.YYYY' )

      if ( _date_val.length != 10 || !_date.isValid() ) {
        errors.push('Неправильная дата.')
      } else {
        if ( _date.diff( today, 'days' ) < 0 ) {
          errors.push('Поле даты начала страхования содержит прошедшую дату.')
        }

        if( this.daysCount < 0 ) {
          errors.push('Дата окончания страхования не может быть раньше, чем дата начала.')
        }

      }

      return errors
    },
    daysCount() {
      var begin_date = moment( this.travel.date_begin, 'DD.MM.YYYY' ),
            end_date = moment( this.travel.date_end, 'DD.MM.YYYY' )


      if ( this.travel.type == 'once' ) {
        if ( !this.beginDateValid || !this.endDateValid )
          return 0

        return end_date.diff( begin_date, 'days' ) + 1
      } else {
        return this.travel.me_days
      }
    },
    fDaysCount(){
      return isNaN(this.daysCount) ? 0 : this.daysCount
    },
    canAddPersonToCalc() {
      return this.calc_pers.length < 5
    },
    canRemovePersonFromCalc() {
      return this.calc_pers.length > 1
    },
    canAddPersonToForm() {
      return this.totalPerson < 5
    },
    canRemovePersonFromForm() {
      return this.polis.insureds.length > 1
    },
    canAddChild(){
      return this.totalPerson < 5
    },
    totalPerson() {
      var _insureds = this.polis.insureds,
          total_adult = _insureds.length

          var act_insurers = []
          for (var i = 0; i < _insureds.length; i++) {
            act_insurers.push(this.getPersonModel(i))
          }

          var childs       = _.map(act_insurers, function(el){ return el.childs.length }),
              total_childs = _.reduce(childs, function(current_total, app_count){ return current_total + app_count }),
              total_person = total_adult + total_childs

          return total_person
    },
    selectedCountriesCodes() {
      return _.map(this.selected_countries, function(el){
        return el.code
      })
    },
    selectedCountriesGroups() {
      var types = _.map(this.selected_countries, function(el){
        return el.type
      })
      return _.uniq(types)
    },
    calculatorErrors() {
      return []
    },
    queryForCalculator() {
        var vm = this

        var query = {
          name_str: $('input[name=name_str]').val(),
          country_groups: this.selectedCountriesGroups,
          days: this.daysCount,
          insureds: _.map(this.calc_pers, function(person){
            return {
              age: person.age,
              sport: person.sport.toString()
            }
          }),
          travel_type: vm.travel.type
        }

      return query
    },
    totalCost() {
      var vm = this

      if ( this.calculator.result == null )
        return '0'

      var total = _.reduce( vm.calculator.result, function(_total, curr, key){
        return _total + parseFloat( curr.prem )
      }, 0)

      return total.toFixed(2)

    },
    totalInsuranceSum() {
      if ( this.calculator.result == null )
        return '0'

      return this.calculator.result.ins_0.sum
    },
    totalCurrency() {
      if ( this.selectedProgram == undefined )
        return 'EUR/USD'

      return this.selectedProgram.rate_name
    },
    totalCurrenceCharacter: function() {
      var currency = this.selectedProgram.rate_name

      if ( currency == 'EUR' )
        return '€'

      if ( currency == 'USD' )
        return '$'

      return ''

    },
    crossLinks() {
      return _.map(this.openedInsuredList, function(person, index){
        if (person.parent_id == undefined)
          return index

        return person.parent_id
      })
    },
    blockRequest() {

      var vm = this

      if ( 
        vm.travel.type == 'once' && vm.daysCount > 91 ||
        vm.travel.type == 'multiple' && (
          vm.daysCount > vm.travel.period ||
          vm.travel.me_days > 180
        )
      ) {
        return true
      }



      return false
    }
  },
  mounted() {  
    var vm = this

    vm.fetchCountries()

    vm.travel.date_begin = moment().add(1, 'days').format('DD.MM.YYYY')
    vm.travel.date_end = moment(vm.travel.date_begin, 'DD.MM.YYYY').add(2, 'days').format('DD.MM.YYYY')

    $(function(){
      vm.setPickerForBeginDate()
    })
  }
})
